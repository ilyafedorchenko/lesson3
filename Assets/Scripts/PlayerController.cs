﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    [SerializeField]
    float speed = 10f, jumpForce = 8f, rotationRate = 100;

    bool isGrounded = false;
    int jumpCount = 0;

    private new Transform transform;
    private new Rigidbody rigidbody;

    // Use this for initialization
    private void Awake ()
    {
        transform = GetComponent<Transform>();
        rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetButton("Vertical")) Run();
        if (Input.GetButton("Horizontal"))
        {
            checkGrounded();
            if (isGrounded) Rotate();
        }
        if (Input.GetButtonDown("Jump")) Jump();
        
    }

    private void Run()
    {
        Vector3 direction = transform.forward * Input.GetAxis("Vertical");
        transform.Translate(direction * speed * Time.deltaTime, Space.World);
    }

    void Rotate()
    {
        Vector3 direction = transform.up * Input.GetAxis("Horizontal");
        transform.Rotate(direction * rotationRate * Time.deltaTime, Space.World);
    }

    void Jump()
    {
        checkGrounded();
        if (isGrounded | jumpCount < 1) 
        {
            rigidbody.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            jumpCount++;
        }
        if (isGrounded && jumpCount > 0) jumpCount = 0;
        Debug.Log("isGrounded: " + isGrounded);
        Debug.Log("jumpCount: " + jumpCount); 
    }

    void checkGrounded()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, 0.3f);
        isGrounded = colliders.Length > 1;
        //Debug.Log("colliders.Length: " + colliders.Length);
        //Debug.Log("transform.position: " + transform.position);
    }
}